/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @author Ahmad Abdelfattah

       @precisions normal z -> s d c
*/

#include "magma_internal.h"
#include "batched_kernel_param.h"

//#define DBG

////////////////////////////////////////////////////////////////////////////////
static void
magma_zpaqrf_batched_tune(
    magma_int_t m, magma_int_t n,
    magma_int_t *nb, magma_int_t *nthreads,
    magma_queue_t queue)
{
    // try different nb values from largest to smallest
    // we cehck the update routine only, since it uses
    // more shared memory than the panel routine
    *nb       = -1;
    *nthreads = -1;
    magma_int_t min_mn = min(m, n);
    if(min_mn <= 32) {
        *nb = min_mn;
        *nthreads = m;
        return;
    }

    magma_int_t ith_start = (m >= 512) ? magma_roundup(m/2, 32) : magma_roundup(m, 32);
    ith_start = min(ith_start, 1024);
    for(magma_int_t inb = min(8,min_mn); inb >= 1; inb /= 2) {
        for(magma_int_t ith = ith_start; ith >= 0; ith-=32) {
            magma_int_t arginfo = magma_zpaqr2_update_fused_sm_batched(
                                    m, n, inb, inb,
                                    NULL, m, NULL,
                                    NULL, NULL,
                                    0, 0, ith, 1,
                                    1, queue );
            if(arginfo == 0) {
                *nb       = inb;
                *nthreads = ith;
                return;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
extern "C" magma_int_t
magma_zpaqrf_batched(
    magma_int_t m, magma_int_t n,
    magmaDoubleComplex** dA_array, magma_int_t ldda,
    magmaDoubleComplex **dtau_array,
    magma_int_t** iflag_array, magma_int_t* info_array,
    double tol, magma_int_t condense,
    magma_int_t batchCount, magma_queue_t queue )
{
    /* Check arguments */
    magma_int_t arginfo = 0;

    if (m < 0)
        arginfo = -1;
    else if (n < 0)
        arginfo = -2;
    else if (ldda < max(1,m))
        arginfo = -4;
    else if (tol < 0)
        arginfo = -8;
    else if ( condense != 0 && condense != 1)
        arginfo = -9;
    else if ( batchCount < 0 )
        arginfo = -10;

    if (arginfo != 0) {
        magma_xerbla( __func__, -(arginfo) );
        return arginfo;
    }

    /* Quick return if possible */
    if (m == 0 || n == 0 || batchCount == 0)
        return arginfo;


    magma_int_t min_mn   = min(m, n);
    magma_int_t nthreads = -1, nb = -1;

    magma_zpaqrf_batched_tune(m, n, &nb, &nthreads, queue);
    if(nb < 1 || nthreads < 1) {
        printf("ERROR: %s cannot be executed on size %lld x %lld, out of resources\n",
                __func__, (long long)m, (long long)n);
    }

    #if defined(DBG)
    magmaDoubleComplex **htau_array = new magmaDoubleComplex*[batchCount];
    magmaDoubleComplex **hA_array   = new magmaDoubleComplex*[batchCount];
    magma_getvector(batchCount, sizeof(magmaDoubleComplex*), (void*)dtau_array, 1, (void*)htau_array, 1, queue);
    magma_getvector(batchCount, sizeof(magmaDoubleComplex*), (void*)dA_array, 1, (void*)hA_array, 1, queue);
    #endif


    for(magma_int_t j = 0; j < min_mn; j += nb) {
        magma_int_t ib = min(nb, min_mn-j);

        //printf("panel: m = %d, ib = %d, nthreads = %d, nb = %d\n", m-j, ib, nthreads, nb);
        arginfo = magma_zpaqr2_fused_sm_batched( m-j, ib,
            dA_array, ldda, dtau_array, iflag_array,
            info_array, nthreads,
            tol, j, condense,
            batchCount, queue );

        #if defined(DBG)
        printf("panel: gbstep = %d\n", j);
        magma_zprint_gpu(m, n, hA_array[0], ldda, queue);
        magma_zprint_gpu(1, min(m,n), htau_array[0], 1, queue);
        #endif

        if( n > (j+ib) ) {
            //printf("update: m = %d, n = %d, ib = %d, nthreads = %d, nb = %d\n", m-j, n-(j+ib), ib, nthreads, nb);
            arginfo = magma_zpaqr2_update_fused_sm_batched(
                m-j, n-(j+ib), nb, ib,
                dA_array, ldda, dtau_array,
                iflag_array, info_array,
                j, condense, nthreads, 0,
                batchCount, queue );
        }

        #if defined(DBG)
        printf("update: gbstep = %d\n", j);
        magma_zprint_gpu(m, n, hA_array[0], ldda, queue);
        magma_zprint_gpu(1, min(m,n), htau_array[0], 1, queue);
        #endif
    }

    #if defined(DBG)
    delete[] htau_array;
    delete[] hA_array;
    #endif

    return arginfo;
}
