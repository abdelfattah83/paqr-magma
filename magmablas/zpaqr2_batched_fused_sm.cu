/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @author Ahmad Abdelfattah

       @precisions normal z -> s d c
*/

#include <cuda.h>    // for CUDA_VERSION
#include "magma_internal.h"
#include "magma_templates.h"
#include "zpaqr2_batched_fused.cuh"
#include "batched_kernel_param.h"

#define PRECISION_z
//#define DBG

////////////////////////////////////////////////////////////////////////////////
template<typename T>
__device__ void print_memory(
                const char* msg,
                int m, int n, T* sA, int lda,
                int tx, int ty, int tz,
                int bx, int by, int bz)
{
#if defined(PRECISION_d) && defined(DBG)
    __syncthreads();
    if(threadIdx.x == tx && threadIdx.y == ty && threadIdx.z == tz &&
       blockIdx.x  == bx && blockIdx.y  == by && blockIdx.z  == bz) {
        printf("%s = [ \n", msg);
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                printf("%8.4f  ", (double)(sA[j*lda+i]));
            }
            printf("\n");
        }
        printf("]; \n");
    }
    __syncthreads();
#endif
}


////////////////////////////////////////////////////////////////////////////////
template<int NTX>
__global__ __launch_bounds__(NTX * NTCOL(NTX))
void
zpaqr2_fused_sm_kernel_batched(
    int M, int N,
    magmaDoubleComplex **dA_array, magma_int_t ldda,
    magmaDoubleComplex **dtau_array,
    magma_int_t** iflag_array, magma_int_t *info_array,
    double tol, magma_int_t gbstep,
    magma_int_t condense, magma_int_t batchCount)
{
#define dAorg(i, j)    dAorg[(j)*ldda + (i)]

    extern __shared__ magmaDoubleComplex zdata[];
    const int tx  = threadIdx.x;
    const int ty  = threadIdx.y;
    const int ntx = blockDim.x;
    const int nty = blockDim.y;
    const int batchid = blockIdx.x * nty + ty;
    if(batchid >= batchCount) return;

    magma_int_t* info  = &info_array[batchid];
    int paqr_rank = (gbstep == 0) ? 0 : (int)(info[0]);

    // get the actual number of rows (add gbstep back to M and then subtract paqr rank)
    int slda      = SLDA( (M+gbstep) );
    M += (gbstep - paqr_rank);

    magmaDoubleComplex* dAorg = dA_array[batchid];
    magmaDoubleComplex* dA    = &dAorg(paqr_rank, gbstep);
    magmaDoubleComplex* dtau  = dtau_array[batchid];
    magma_int_t* iflag        = iflag_array[batchid] + gbstep;

    // shared memory pointers
    magmaDoubleComplex* sA    = (magmaDoubleComplex*)(zdata);
    magmaDoubleComplex* sY    = sA   + (nty * slda * N);
    magmaDoubleComplex* stau  = sY   + (nty * N);
    magmaDoubleComplex* sTmp  = stau + nty * N;
    sA    += ty * slda * N;
    sY    += ty * N;
    stau  += ty * N;
    sTmp  += ty * ntx;
    double* snorm = (double*) (sTmp); // must be set after offsetting w.r.t. ty

    magmaDoubleComplex alpha, tau, tmp, scale = MAGMA_Z_ZERO;
    double norm = MAGMA_D_ZERO, beta;

    int linfo = paqr_rank, good_cols = 0;

    // decide where the output is written
    // if condense = 0, dOut = dA
    // if condense = 1, dOut should start at the rank of
    // the already-factorized preceding sub-matrix
    magmaDoubleComplex* dOut = (condense == 0) ? dA : &dAorg(paqr_rank, paqr_rank);

    dtau += (condense) ? paqr_rank : gbstep;

    // init tau
    if(tx < N) {
        stau[tx]  = MAGMA_Z_ZERO;
        iflag[tx] = 0;
    }

    // read
    for(int j = 0; j < N; j++){
        for(int i = tx; i < M; i+=ntx) {
            sA(i,j) = dA[ j * ldda + i ];
        }
    }
    __syncthreads();

    int ir = 0, jc = 0;
    for(int j = 0; j < N; j++){
        alpha = sA(ir,j);

        zpaqr2_compute_norm(M-ir, &sA(ir,j), snorm, tx, ntx);
        // there is a sync at the end of zpaqr2_compute_norm

        norm = sqrt(snorm[0]);

        // check the norm
        if(norm < tol) {
            iflag[j] = 1;
            // if condensed output is required, we don't increment ir, so
            // the current column in global memory will be overwritten by
            // the next "good column"
            // otherwise, we increment it to leave the current column unchanged
            // in global memory
            jc += (condense > 0) ? 0 : 1;
            //continue;
        }
        else {
            good_cols++;
            beta = -copysign(norm, real(alpha));
            scale = MAGMA_Z_DIV( MAGMA_Z_ONE,  alpha - MAGMA_Z_MAKE(beta, 0));
            tau = MAGMA_Z_MAKE( (beta - real(alpha)) / beta, -imag(alpha) / beta );

            if(tx == 0) {
                stau[jc] = tau;
                sA(ir,j) = MAGMA_Z_ONE;
            }

            // scale the current column below the ir
            for(int i = (tx+ir+1); i < M; i+=ntx) {
                sA(i,j) *= scale;
            }
            __syncthreads();

            // copy the first portion of the column into tmp
            // since M > N and ntx >= N, this portion must
            // have the diagonal
            tmp = (tx == ir) ? MAGMA_Z_MAKE(beta, MAGMA_D_ZERO) : sA(tx, j);

            // write the column into global memory
            /*
            dOut[jc * ldda + tx] = tmp;
            for(int i = tx+ntx; i < M; i+=ntx) {
                dOut[ jc * ldda + i ] = sA(i, j);
            }
            */
            for(int i = tx; i < M; i+=ntx) {
                dOut[ jc * ldda + i ] = (i == tx) ? tmp : sA(i, j);
            }

            // now compute (I - tau * v * v') A
            // first: y = tau * v' * A (row vector)
            zpaqr2_compute_vtA_device(M, N, j, ir, sA, slda, sY, tau, sTmp, tx, ntx);
            __syncthreads();

            // now compute: A = A - v * y
            for(int jj = j+1; jj < N; jj++){
                for(int i = tx+ir; i < M; i+=ntx) {
                    sA(i,jj) -= sA(i,j) * sY[jj];
                }
            }
            __syncthreads();
            ir++;
            jc++;
        }
    }

    // write tau
    if(tx < N) {
        dtau[tx] = stau[tx];
    }

    // write info
    if( tx == 0 ){
        (*info) = (magma_int_t)(linfo + good_cols);
    }

    // in condensed mode, we should condense the columns above
    // the current panel
    if( condense == 1 ) {
        magmaDoubleComplex *dAtmp = &dAorg(0,gbstep);
        magmaDoubleComplex *dOtmp = &dAorg(0,paqr_rank);
        for(int i = tx; i < paqr_rank; i+=ntx) {
            int jc = 0;
            for(int j = 0; j < N; j++) {
                if(iflag[j] == 0) {
                    //printf("row %d, moving column %d --> %d\n", i, j, jc);
                    dOtmp[jc * ldda + i] = dAtmp[j * ldda + i];
                    jc++;
                }
            }
        }
    }

#undef dAorg
}

////////////////////////////////////////////////////////////////////////////////
template<int NTX>
magma_int_t
magma_zpaqr2_fused_sm_batched_kernel_driver(
    magma_int_t m, magma_int_t n,
    magmaDoubleComplex** dA_array, magma_int_t ldda,
    magmaDoubleComplex **dtau_array, magma_int_t** iflag_array,
    magma_int_t* info_array, double tol,
    magma_int_t gbstep, magma_int_t condense,
    magma_int_t batchCount, magma_queue_t queue )
{
    magma_int_t arginfo = 0;
    magma_device_t device;
    magma_getdevice( &device );

    magma_int_t nthreads = NTX;

    const magma_int_t ntcol = NTCOL(nthreads);
    // for caching a panel of A, we have to account for the largest rank possible
    // in the batch, this is equal to (m+gbstep), because `m` is passed according to
    // a full rank factorization (original m - gbstep). The largest rank case, however,
    // is when all preceding columns are zeros,
    // and the current panel has full rank, so (m+gbstep)
    magma_int_t shmem = ( SLDA((m+gbstep) ) * n * sizeof(magmaDoubleComplex) );
    shmem            += ( n        * sizeof(magmaDoubleComplex) );  // sY
    shmem            += ( n        * sizeof(magmaDoubleComplex) );  // stau
    shmem            += ( nthreads * sizeof(magmaDoubleComplex) );  // used for snorm and for computing v' * A
    shmem            *= ntcol;

    magma_int_t gridx = magma_ceildiv(batchCount, ntcol);
    dim3 grid(gridx, 1, 1);
    dim3 threads( nthreads, ntcol, 1);

    // get max. dynamic shared memory on the GPU
    magma_int_t nthreads_max, shmem_max = 0;
    cudaDeviceGetAttribute (&nthreads_max, cudaDevAttrMaxThreadsPerBlock, device);
    #if CUDA_VERSION >= 9000
    cudaDeviceGetAttribute (&shmem_max, cudaDevAttrMaxSharedMemoryPerBlockOptin, device);
    if (shmem <= shmem_max) {
        cudaFuncSetAttribute(zpaqr2_fused_sm_kernel_batched<NTX>, cudaFuncAttributeMaxDynamicSharedMemorySize, shmem);
    }
    #else
    cudaDeviceGetAttribute (&shmem_max, cudaDevAttrMaxSharedMemoryPerBlock, device);
    #endif    // CUDA_VERSION >= 9000

    magma_int_t total_threads = nthreads * ntcol;
    if ( total_threads > nthreads_max || shmem > shmem_max ) {
        //printf("error: kernel %s requires too many threads (%lld) or too much shared memory (%.2f KB)\n",
        //        __func__, (long long)total_threads, (double)(shmem/1024.));
        arginfo = -100;
        return arginfo;
    }

    void *kernel_args[] = {&m, &n, &dA_array, &ldda, &dtau_array, &iflag_array, &info_array, &tol, &gbstep, &condense, &batchCount};
    cudaError_t e = cudaLaunchKernel((void*)zpaqr2_fused_sm_kernel_batched<NTX>, grid, threads, kernel_args, shmem, queue->cuda_stream());
    if( e != cudaSuccess ) {
        //printf("error in %s : failed to launch kernel %s\n", __func__, cudaGetErrorString(e));
        arginfo = -100;
    }

    return arginfo;
}

////////////////////////////////////////////////////////////////////////////////
extern "C" magma_int_t
magma_zpaqr2_fused_sm_batched(
    magma_int_t m, magma_int_t n,
    magmaDoubleComplex** dA_array, magma_int_t ldda,
    magmaDoubleComplex **dtau_array, magma_int_t** iflag_array,
    magma_int_t* info_array, magma_int_t nthreads,
    double tol, magma_int_t gbstep, magma_int_t condense,
    magma_int_t batchCount, magma_queue_t queue )
{
    magma_int_t arginfo = 0;

    if (m < 0)
        arginfo = -1;
    else if (n < 0 || n > m) {
        printf("ERROR: function %s requires than m >= n\n", __func__);
        arginfo = -2;
    }
    else if (ldda < max(1,m))
        arginfo = -4;

    if (arginfo != 0) {
        magma_xerbla( __func__, -(arginfo) );
        return arginfo;
    }

    /* Quick return if possible */
    if (m == 0 || n == 0)
        return arginfo;

    nthreads = min(nthreads, m);
    magma_int_t nthreads32 = magma_roundup(nthreads, 32);

    switch(nthreads32) {
        case   32: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver<  32>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case   64: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver<  64>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case   96: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver<  96>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  128: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 128>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  160: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 160>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  192: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 192>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  224: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 224>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  256: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 256>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  288: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 288>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  320: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 320>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  352: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 352>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  384: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 384>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  416: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 416>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  448: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 448>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  480: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 480>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  512: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 512>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  544: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 544>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  576: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 576>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  608: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 608>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  640: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 640>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  672: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 672>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  704: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 704>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  736: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 736>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  768: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 768>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  800: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 800>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  832: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 832>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  864: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 864>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  896: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 896>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  928: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 928>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  960: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 960>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case  992: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver< 992>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        case 1024: arginfo = magma_zpaqr2_fused_sm_batched_kernel_driver<1024>(m, n, dA_array, ldda, dtau_array, iflag_array, info_array, tol, gbstep, condense, batchCount, queue ); break;
        default: arginfo = -100;
    }

    return arginfo;
}
