/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @author Ahmad Abdelfattah

       @precisions normal z -> s d c
*/

#include "magma_internal.h"
#include "magma_templates.h"
#include "batched_kernel_param.h"
#include "zpaqr2_batched_fused.cuh"

#define PRECISION_z
//#define DBG

////////////////////////////////////////////////////////////////////////////////
#define SLDA(n)              ( (((n)+1)%4) == 0 ? (n) : (n+1) )
#define sA(i,j)               sA[(j) * slda + (i)]
#define sV(i,j)               sV[(j) * sldv + (i)]
#define sT(i,j)               sT[(j) * sldt + (i)]

////////////////////////////////////////////////////////////////////////////////
template<typename T>
__device__ void print_memory(
                const char* msg,
                int m, int n, T* sA, int lda,
                int tx, int ty, int tz,
                int bx, int by, int bz)
{
#if defined(PRECISION_d) && defined(DBG)
    __syncthreads();
    if(threadIdx.x == tx && threadIdx.y == ty && threadIdx.z == tz &&
       blockIdx.x  == bx && blockIdx.y  == by && blockIdx.z  == bz) {
        printf("%s = [ \n", msg);
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                printf("%8.4f  ", (double)(sA[j*lda+i]));
            }
            printf("\n");
        }
        printf("]; \n");
    }
    __syncthreads();
#endif
}

////////////////////////////////////////////////////////////////////////////////
template<int NTX>
__global__
__launch_bounds__(NTX * NTCOL(NTX))
void
zpaqr2_update_sm_kernel_batched(
    int m, int n, int nb, int ib,
    magmaDoubleComplex **dA_array, int ldda,
    magmaDoubleComplex **dtau_array, magma_int_t** iflag_array,
    magma_int_t *info_array, int gbstep,
    bool is_condensed, int batchCount )
{
#define dAorg(i, j)    dAorg[(j)*ldda + (i)]
#define dA(i, j)       dA[(j)*ldda + (i)]

    extern __shared__ magmaDoubleComplex zdata[];
    const int tx  = threadIdx.x;
    const int ty  = threadIdx.y;
    const int ntx = blockDim.x;
    const int nty = blockDim.y;
    const int tpc = ntx / nb; // a minimum of nb threads is required
    const int ty_ = tx / tpc;
    const int tx_ = tx % tpc;
    const int batchid = blockIdx.x * nty + ty;
    if(batchid >= batchCount) return;

    magmaDoubleComplex* dAorg = dA_array[batchid];
    magmaDoubleComplex* dtau  = dtau_array[batchid];
    magma_int_t* iflag        = iflag_array[batchid];
    int paqr_rank             = (int)(info_array[batchid]);

    // shared memory pointers
    const int slda = SLDA(m+gbstep);
    const int sldv = SLDA(m+gbstep);
    const int sldt = SLDA(tpc);
    magmaDoubleComplex* sV    = (magmaDoubleComplex*)(zdata);
    magmaDoubleComplex* sA    = sV    + (nty * sldv * nb);
    magmaDoubleComplex* sT    = sA    + (nty * slda * nb);
    magmaDoubleComplex* stau2 = sT    + (nty * sldt * nb);
    magmaDoubleComplex* stau  = stau2 + (nty * nb);
    int*                sflag = (int*)(stau + (nty * nb));
    sV    += ty * sldv * nb;
    sA    += ty * slda * nb;
    sT    += ty * sldt * nb;
    stau2 += ty * nb;
    stau  += ty * nb;
    sflag += ty * nb;

    // advance iflag get the rank of the panel
    iflag += gbstep;
    for(int i = tx; i < ib; i+=ntx) {
        sflag[i] = iflag[i];
    }
    __syncthreads();

    int panel_rank = 0;
    for(int i = 0; i < ib; i++) {
        panel_rank += (1 - sflag[i]);
    }
    __syncthreads();

    // get previous paqr_rank (excluding the current panel being applied)
    int prev_paqr_rank = paqr_rank - panel_rank;

    // if panel_rank is zero, return (no reflectors to apply)
    if(panel_rank == 0) return;

    // advance dtau properly
    dtau  += (is_condensed) ? prev_paqr_rank : gbstep;

    // determine where the reflectors (dV) are
    magmaDoubleComplex* dV = (is_condensed) ? &dAorg(prev_paqr_rank, prev_paqr_rank) :
                                              &dAorg(prev_paqr_rank, gbstep);

    // reduce m by the prev_paqr_rank
    m += (gbstep - prev_paqr_rank);

    #if defined(DBG) && defined(PRECISION_d)
    __syncthreads();
    if(tx == 0) {
        printf("dA = dAorg(%d, %d)\n", prev_paqr_rank, gbstep+nb);
        printf("m = %d, slda = %d\n", m, slda);
    }
    __syncthreads();
    #endif
    // determine where the trailing matrix starts
    magmaDoubleComplex* dA  = &dAorg(prev_paqr_rank, gbstep+nb);

    magmaDoubleComplex zsum;
    int iib;

    // init stau, stau2 to zero
    if(tx < nb) {
        stau[tx]  = MAGMA_Z_ZERO;
        stau2[tx] = MAGMA_Z_ZERO;
    }

    // init sA,sV to zero
    for(int i = tx; i < m; i+=ntx) {
        for(int j = 0; j < nb; j++) {
            sA(i,j) = MAGMA_Z_ZERO;
            sV(i,j) = MAGMA_Z_ZERO;
        }
    }

    // read tau and init diag(sV)
    if(tx < panel_rank) {
        sV(tx,tx) = MAGMA_Z_ONE; // does not need a sync before it
    }

    // read into sV and stau
    if(is_condensed) {
        // read dtau into stau
        if(tx < panel_rank) {
            stau[tx] = dtau[tx];
        }

        // first loop checks against the diagonal
        for(int j = 0; j < panel_rank; j++) {
            sV(tx,j) = (tx > j && tx < m) ? dV[j * ldda + tx] : sV(tx,j);
        }
        __syncthreads();

        for(int i = tx+ntx; i < m; i+=ntx) {
            for(int j = 0; j < panel_rank; j++) {
                sV(i,j) = dV[j * ldda + i];
            }
        }
        __syncthreads();
    }
    else{
        // read dtau into stau2 and condense it into stau
        if(tx < ib) {
            stau2[tx] = dtau[tx];
        }
        __syncthreads();

        int jx = 0;
        if(tx == 0) {
            for(int i=0; i < ib; i++) {
                stau[jx] = (sflag[i] == 0) ? stau2[i]: MAGMA_Z_ZERO;
                jx += (1 - sflag[i]);
            }
        }

        jx = 0;
        // first loop over nb checks against the diagonal
        for(int j = 0; j < ib; j++) {
            int good_col = 1 - sflag[j];
            sV(tx,jx) = (good_col == 1 && tx > jx && tx < m) ? dV[j * ldda + tx] : sV(tx,jx);
            jx += good_col;
        }

        for(int i = tx+ntx; i < m; i+=ntx) {
            jx = 0;
            for(int j = 0; j < ib; j++) {
                int good_col = 1 - sflag[j];
                sV(i,jx) = (good_col == 1) ? dV[j * ldda + i] : MAGMA_Z_ZERO;
                jx += good_col;
            }
        }
    }
    // end of reading in SV

    //print_memory<magmaDoubleComplex>("sV", m, nb, sV, sldv, 0, 0, 0, 0, 0, 0);

    //print_memory<magmaDoubleComplex>("dA1", m, nb, dA, ldda, 0, 0, 0, 0, 0, 0);

    //////////// main loop ////////////////
    for(iib = 0; iib < (n/nb)*nb; iib+=nb) {
        // read A
        for(int i = tx; i < m; i+=ntx) {
            for(int j = 0; j < nb; j++) {
                sA(i, j) = dA[ j * ldda + i ];
            }
        }
        __syncthreads();

        //print_memory<magmaDoubleComplex>("sA1", m, nb, sA, slda, 0, 0, 0, 0, 0, 0);

        // apply loop
        for(int j = 0; j < panel_rank; j++) {
            // compute v' * A and reduce (1-of-2)
            zsum = MAGMA_Z_ZERO;
            if(ty_ < nb) {
                for(int i = tx_; i < m; i+=tpc) {
                    zsum += sA(i,ty_) * MAGMA_Z_CONJ( sV(i,j) );
                }
                sT(tx_,ty_) = zsum;
            }
            __syncthreads();

            // reduce (2-of-2)
            zsum = MAGMA_Z_ZERO;
            if(tx < nb) {
                for(int i = 0; i < tpc; i++) {
                    zsum += sT(i,tx);
                }
                sT(0,tx) = MAGMA_Z_CONJ( stau[j] ) * zsum;
            }
            __syncthreads();

            // rank update
            for(int i = tx; i < m; i+=ntx) {
                for(int jj = 0; jj < nb; jj++) {
                    sA(i,jj) -= sV(i,j) * sT(0,jj);
                }
            }
            __syncthreads();
        }    // end of apply loop

        //print_memory<magmaDoubleComplex>("sA2", m, nb, sA, slda, 0, 0, 0, 0, 0, 0);

        // write sA
        for(int i = tx; i < m; i+=ntx) {
            for(int j = 0; j < nb; j++) {
                dA[ j * ldda + i ] = sA(i, j);
            }
        }

        // advance dA
        dA += nb*ldda;
    }    // end of main loop

    //////////// cleanup section ////////////////
    if(n - iib > 0) {
        int nn = n - iib;
        // read A
        for(int i = tx; i < m; i+= ntx) {
            for(int j = 0; j < nn; j++) {
                sA(i,j) = dA[ j * ldda + i ];
            }
        }
        __syncthreads();

        //print_memory<magmaDoubleComplex>("sA3", m, nb, sA, slda, 0, 0, 0, 0, 0, 0);

        // apply loop
        for(int j = 0; j < panel_rank; j++) {
            // reduce (1-of-2)
            zsum = MAGMA_Z_ZERO;
            if(ty_ < nn) {
                for(int i = tx_; i < m; i+=tpc) {
                    zsum += sA(i,ty_) * MAGMA_Z_CONJ( sV(i,j) );
                }

                sT(tx_,ty_) = zsum;
            }
            __syncthreads();

            // reduce (2-of-2)
            zsum = MAGMA_Z_ZERO;
            if(tx < nn) {
                for(int i = 0; i < tpc; i++) {
                    zsum += sT(i,tx);
                }
                sT(0,tx) = MAGMA_Z_CONJ( stau[j] ) * zsum;
            }
            __syncthreads();

            // rank update
            for(int i = tx; i < m; i+=ntx) {
                for(int jj = 0; jj < nn; jj++) {
                    sA(i,jj) -= sV(i,j) * sT(0,jj);
                }
            }
            __syncthreads();

        }    // end of apply loop

        //print_memory<magmaDoubleComplex>("sA4", m, nb, sA, slda, 0, 0, 0, 0, 0, 0);

        // write rA
        for(int i = tx; i < m; i+=ntx) {
            for(int j = 0; j < nn; j++) {
                dA[ j * ldda + i ] = sA(i,j);
            }
        }
    }    // end of cleanup section

#undef dAorg
#undef dA
}

////////////////////////////////////////////////////////////////////////////////
template<int NTX>
static magma_int_t
zpaqr2_update_sm_kernel_driver_batched(
    magma_int_t m, magma_int_t n, magma_int_t nb, magma_int_t ib,
    magmaDoubleComplex **dA_array, magma_int_t ldda,
    magmaDoubleComplex **dtau_array,
    magma_int_t** iflag_array, magma_int_t *info_array,
    magma_int_t gbstep, magma_int_t is_condensed,
    magma_int_t check_launch_only, magma_int_t batchCount, magma_queue_t queue )
{
    magma_device_t device;
    magma_getdevice( &device );
    magma_int_t arginfo = 0;

    magma_int_t nthreads = NTX;
    magma_int_t ntcol   = NTCOL(NTX);
    magma_int_t TPC     = nthreads / nb;

    magma_int_t shmem = 0;
    shmem += SLDA(m+gbstep) * nb * sizeof(magmaDoubleComplex);  // sA
    shmem += SLDA(m+gbstep) * nb * sizeof(magmaDoubleComplex);  // sV
    shmem += SLDA(TPC) * nb * sizeof(magmaDoubleComplex);  // sT
    shmem += nb * 2         * sizeof(magmaDoubleComplex);  // stau and stau2
    shmem += nb             * sizeof(int);                 // sflag
    shmem *= ntcol;

    magma_int_t gridx = magma_ceildiv(batchCount, ntcol);

    dim3 grid(gridx, 1, 1);
    dim3 threads( nthreads, ntcol, 1);

    // get max. dynamic shared memory on the GPU
    int nthreads_max, shmem_max = 0;
    cudaDeviceGetAttribute (&nthreads_max, cudaDevAttrMaxThreadsPerBlock, device);
    #if CUDA_VERSION >= 9000
    cudaDeviceGetAttribute (&shmem_max, cudaDevAttrMaxSharedMemoryPerBlockOptin, device);
    if (shmem <= shmem_max) {
        cudaFuncSetAttribute(zpaqr2_update_sm_kernel_batched<NTX>, cudaFuncAttributeMaxDynamicSharedMemorySize, shmem);
    }
    #else
    cudaDeviceGetAttribute (&shmem_max, cudaDevAttrMaxSharedMemoryPerBlock, device);
    #endif    // CUDA_VERSION >= 9000

    magma_int_t total_threads = nthreads * ntcol;
    if ( total_threads > nthreads_max || shmem > shmem_max ) {
        //printf("error: kernel %s requires too many threads or too much shared memory\n", __func__);
        arginfo = -100;
        return arginfo;
    }

    if( check_launch_only == 1 ) return arginfo;

    bool is_condensed_b = (is_condensed <= 0) ? false : true;
    void *kernel_args[] = {&m, &n, &nb, &ib, &dA_array, &ldda, &dtau_array, &iflag_array, &info_array, &gbstep, &is_condensed_b, &batchCount};
    cudaError_t e = cudaLaunchKernel((void*)zpaqr2_update_sm_kernel_batched<NTX>, grid, threads, kernel_args, shmem, queue->cuda_stream());
    if( e != cudaSuccess ) {
        //printf("error in %s : failed to launch kernel %s\n", __func__, cudaGetErrorString(e));
        arginfo = -100;
    }

    return arginfo;
}

////////////////////////////////////////////////////////////////////////////////
extern "C"
magma_int_t
magma_zpaqr2_update_fused_sm_batched(
    magma_int_t m, magma_int_t n, magma_int_t nb, magma_int_t ib,
    magmaDoubleComplex **dA_array, magma_int_t ldda,
    magmaDoubleComplex **dtau_array,
    magma_int_t** iflag_array, magma_int_t *info_array,
    magma_int_t gbstep, magma_int_t is_condensed,
    magma_int_t nthreads, magma_int_t check_launch_only,
    magma_int_t batchCount, magma_queue_t queue )
{
    magma_int_t arginfo = 0;
    magma_int_t m32 = magma_roundup(m, 32);

    nthreads = min(nthreads, m);   // nthreads should not be greater than m
    nthreads = max(nthreads, nb);  // nthreads should not be less than nb

    if (m32 < nb)
        arginfo = -1;
    else if (n < 0)
        arginfo = -2;
    else if (ldda < max(1,m))
        arginfo = -4;

    /* Quick return if possible */
    if (m == 0 || n == 0)
        return arginfo;

    if (arginfo != 0) {
        magma_xerbla( __func__, -(arginfo) );
        return arginfo;
    }

    magma_int_t nthreads32 = magma_roundup(nthreads, 32);

    switch(nthreads32) {
        case   32: arginfo = zpaqr2_update_sm_kernel_driver_batched<  32>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case   64: arginfo = zpaqr2_update_sm_kernel_driver_batched<  64>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case   96: arginfo = zpaqr2_update_sm_kernel_driver_batched<  96>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  128: arginfo = zpaqr2_update_sm_kernel_driver_batched< 128>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  160: arginfo = zpaqr2_update_sm_kernel_driver_batched< 160>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  192: arginfo = zpaqr2_update_sm_kernel_driver_batched< 192>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  224: arginfo = zpaqr2_update_sm_kernel_driver_batched< 224>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  256: arginfo = zpaqr2_update_sm_kernel_driver_batched< 256>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  288: arginfo = zpaqr2_update_sm_kernel_driver_batched< 288>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  320: arginfo = zpaqr2_update_sm_kernel_driver_batched< 320>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  352: arginfo = zpaqr2_update_sm_kernel_driver_batched< 352>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  384: arginfo = zpaqr2_update_sm_kernel_driver_batched< 384>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  416: arginfo = zpaqr2_update_sm_kernel_driver_batched< 416>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  448: arginfo = zpaqr2_update_sm_kernel_driver_batched< 448>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  480: arginfo = zpaqr2_update_sm_kernel_driver_batched< 480>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  512: arginfo = zpaqr2_update_sm_kernel_driver_batched< 512>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  544: arginfo = zpaqr2_update_sm_kernel_driver_batched< 544>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  576: arginfo = zpaqr2_update_sm_kernel_driver_batched< 576>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  608: arginfo = zpaqr2_update_sm_kernel_driver_batched< 608>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  640: arginfo = zpaqr2_update_sm_kernel_driver_batched< 640>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  672: arginfo = zpaqr2_update_sm_kernel_driver_batched< 672>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  704: arginfo = zpaqr2_update_sm_kernel_driver_batched< 704>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  736: arginfo = zpaqr2_update_sm_kernel_driver_batched< 736>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  768: arginfo = zpaqr2_update_sm_kernel_driver_batched< 768>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  800: arginfo = zpaqr2_update_sm_kernel_driver_batched< 800>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  832: arginfo = zpaqr2_update_sm_kernel_driver_batched< 832>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  864: arginfo = zpaqr2_update_sm_kernel_driver_batched< 864>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  896: arginfo = zpaqr2_update_sm_kernel_driver_batched< 896>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  928: arginfo = zpaqr2_update_sm_kernel_driver_batched< 928>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  960: arginfo = zpaqr2_update_sm_kernel_driver_batched< 960>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case  992: arginfo = zpaqr2_update_sm_kernel_driver_batched< 992>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        case 1024: arginfo = zpaqr2_update_sm_kernel_driver_batched<1024>( m, n, nb, ib, dA_array, ldda, dtau_array, iflag_array, info_array, gbstep, is_condensed, check_launch_only, batchCount, queue ); break;
        default: arginfo = -100;
    }
    return arginfo;
}
